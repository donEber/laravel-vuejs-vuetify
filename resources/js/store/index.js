import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        citas: [
            { autor: 'Aqui', cita: '' },
            { autor: 'Algo', cita: '' },
            { autor: 'Anda', cita: 'mal' }
        ],
        indiceEditar:null
    },
    mutations: {
        async agregarCita(state, cita) {
            await axios.post(`http://127.0.0.1:8000/cita/`, cita)
                .then((res) => { state.citas.push(res.data)})
        },
        async eliminarCita(state, idindex) {
            console.log(`Id = ${idindex.id}, index=${idindex.index}`)
            await axios.delete(`http://127.0.0.1:8000/cita/${idindex.id}`).then(() => {
                state.citas.splice(idindex.index, 1);
            });
        },
        async editarCita(state, cita) {
            const paramsCita = {autor:cita.autor, cita: cita.cita}
            await axios.put(`http://127.0.0.1:8000/cita/${cita.id}`, paramsCita)
                .then((res) => {
                    //state.citas.push(res.data)
                    console.log("Todo bien muchacho");
                    console.log(state.citas[0]);
                    const index = state.citas.findIndex(item => item.id === cita.id);
                    console.log(`El index es: ${index}`)
                    state.citas[index] = res.data;
                    console.log("datos:");
                    console.log(res.data);
                    
                    
                    })
        },
        llenarCitas(state, citasObtenidas) {
            state.citas = citasObtenidas;
        },
    },
    actions: {
        obtenerCitas: async function ({ commit }) {
            const citasObtenidas = await axios.get("http://127.0.0.1:8000/cita")
                .then(res => res.data);
            commit('llenarCitas', citasObtenidas)
        }
    },
    modules: {
    }
})
