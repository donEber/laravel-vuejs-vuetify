require('./bootstrap');
window.Vue = require('vue');

import store from './store'
import router from './routes'
import vuetify from "../js/plugins/vuetify"

Vue.component('app-view', require('./views/App.vue').default);
Vue.component('cita-component', require('./components/CitaComponent.vue').default);
Vue.component('cita-agregar-component', require('./components/CitaAgregarComponent.vue').default);
const app = new Vue({
    el: '#app',
    store,
    router,
    vuetify
});
