<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="250">
    <img src="https://vuejs.org/images/logo.png" alt="Build Status" width="100">
    <img src="https://cdn.vuetifyjs.com/images/logos/vuetify-logo-dark.png" alt="Build Status" width="100">
</p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
</p>

# Pasos para la instalación

1. Descarga o clona el repositorio y entra en el proyecto
2. En el archivo ***.env*** (copia y renombra el archivo ***.env.example***) modifica la base de datos de los sig. datos (y los que sean necesarios mas):
    - `DB_DATABASE=laravel`
    - `DB_USERNAME=root`
    - `DB_PASSWORD=  `
3. Ejecuta el siguiente comando `composer update`
4. Ejecuta el comando `php artisan key:generate`
5. Ejecuta `npm install
6. Ahora puedes generar las migraciones con `php artisan migrate`
7. Si se tuvieras seeders `php artisan db:seed`

**Y listo, ya puedes correr el proyecto con normalidad con `php artisan serve`**
